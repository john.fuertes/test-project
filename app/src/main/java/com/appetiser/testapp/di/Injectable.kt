package com.appetiser.testapp.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
