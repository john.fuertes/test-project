package com.appetiser.baseplate.domain.ext

import android.content.Context
import android.telephony.TelephonyManager
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil


fun Context.countryCode(): Int {
    val telephonyManager = this.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    return PhoneNumberUtil.createInstance(this).getCountryCodeForRegion(telephonyManager.simCountryIso.toUpperCase())
}
