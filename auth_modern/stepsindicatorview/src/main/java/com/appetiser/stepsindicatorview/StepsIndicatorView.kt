package com.appetiser.stepsindicatorview

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat

class StepsIndicatorView : FrameLayout, ViewTreeObserver.OnPreDrawListener {


    private var stepCount: Int = 4
    private var stepNumberSelected: Int = 0
    private var selectedBackground: Int = R.drawable.colored_step_indicator
    private var unselectedBackground: Int = R.drawable.unselected_step_indicator
    private var dividerColor: Int = ContextCompat.getColor(context, R.color.white)
    private var actualHeight = 12

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.StepsIndicatorView)
            try {
                array.getInt(R.styleable.StepsIndicatorView_stepsCount, 4).let {
                    stepCount = it
                }
                array.getInt(R.styleable.StepsIndicatorView_stepsNumberSelected, stepCount).let {
                    stepNumberSelected = it
                }
                array.getInt(R.styleable.StepsIndicatorView_selectedBackground, R.drawable.colored_step_indicator).let {
                    selectedBackground = it
                }
                array.getInt(R.styleable.StepsIndicatorView_unselectedBackground, R.drawable.unselected_step_indicator).let {
                    unselectedBackground = it
                }

                array.getColor(R.styleable.StepsIndicatorView_dividerColor, ContextCompat.getColor(context, R.color.white)).let {
                    dividerColor = it
                }
            } finally {
                array.recycle()
            }
        }

        if (stepNumberSelected > stepCount) {
            throw IndexOutOfBoundsException("StepNumber must be lesser than StepSize")
        } else if (stepCount <= 1) {
            throw IndexOutOfBoundsException("Step count must be more than 1")
        }

        if (stepNumberSelected == 0) {
            stepNumberSelected = stepCount
        }

        viewTreeObserver.addOnPreDrawListener(this)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    fun setStepNumberSelected(stepNumberSelected: Int) {
        this.stepNumberSelected = stepNumberSelected

        removeAllViewsInLayout()

        setupUnselectedBackground()
        setupDividerBackground()

        Toast.makeText(context, "Child count = $childCount", Toast.LENGTH_SHORT).show()
    }

    private fun setupUnselectedBackground() {
        // for divider
        val unselectedContainer = LinearLayout(context)

        unselectedContainer.setBackgroundResource(unselectedBackground)
        val percentage = ((((WEIGHT_SUM / stepCount)) * 0.01) * stepNumberSelected)
        val shit = width - (width * percentage)
        val unselectedContainerLayoutParams = FrameLayout.LayoutParams(shit.toInt(), actualHeight)
        unselectedContainerLayoutParams.gravity = Gravity.CENTER or Gravity.END
        unselectedContainer.layoutParams = unselectedContainerLayoutParams

        addView(unselectedContainer)

    }

    private fun setupDividerBackground() {
        val tempContainerForDivider = LinearLayout(context)
        tempContainerForDivider.setBackgroundColor(Color.TRANSPARENT)
        tempContainerForDivider.orientation = LinearLayout.HORIZONTAL
        tempContainerForDivider.weightSum = WEIGHT_SUM
        tempContainerForDivider.gravity = Gravity.CENTER
        val tempContainerLayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, actualHeight)
        tempContainerLayoutParams.gravity = Gravity.CENTER
        tempContainerForDivider.layoutParams = tempContainerLayoutParams

        for (i in 1 until stepCount) {
            val viewContainer = FrameLayout(context)
            viewContainer.setBackgroundColor(Color.TRANSPARENT)
            val viewContainerLayoutParams = LinearLayout.LayoutParams(0, actualHeight)
            viewContainerLayoutParams.weight = (WEIGHT_SUM / stepCount)
            viewContainer.layoutParams = viewContainerLayoutParams

            val divider = View(context)
            val dividerLayoutParams = FrameLayout.LayoutParams(10, actualHeight)
            divider.setBackgroundColor(dividerColor)
            dividerLayoutParams.gravity = Gravity.CENTER
            divider.layoutParams = dividerLayoutParams

            viewContainer.addView(divider)
            tempContainerForDivider.addView(viewContainer)
        }

        addView(tempContainerForDivider)
    }

    override fun onPreDraw(): Boolean {
        viewTreeObserver.removeOnPreDrawListener(this)

        setBackgroundResource(selectedBackground)
        setupUnselectedBackground()
        setupDividerBackground()
        return false
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
    }

    companion object {
        private const val WEIGHT_SUM = 100f
    }

}
