package com.appetiser.testapp.features.auth.register.accountdetail

import com.appetiser.testapp.data.poko.UserSession


sealed class AccountDetailsState {

    data class FetchUserDetails(val user: UserSession) : AccountDetailsState()

    data class Error(val throwable: Throwable) : AccountDetailsState()

    object ShowProgressLoading : AccountDetailsState()

    object HideProgressLoading : AccountDetailsState()

}
