package com.appetiser.baseplate.domain.response.auth

import com.appetiser.baseplate.domain.models.APIUser

open class UserDataResponse(
    val user: APIUser,
    val token: String = ""
)
