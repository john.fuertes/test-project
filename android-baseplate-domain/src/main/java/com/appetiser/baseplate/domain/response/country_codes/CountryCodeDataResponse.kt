package com.appetiser.baseplate.domain.response.country_codes

import com.appetiser.baseplate.domain.models.APICountryCode

open class CountryCodeDataResponse(
        val countries: List<APICountryCode>
)
