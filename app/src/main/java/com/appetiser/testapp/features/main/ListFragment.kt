package com.appetiser.testapp.features.main

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.testapp.R
import com.appetiser.testapp.base.BaseFragment
import com.appetiser.testapp.data.vo.Track
import com.appetiser.testapp.ext.ensureNavigate
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : BaseFragment(R.layout.fragment_list), ListController.Callback {

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ViewModel::class.java)
    }

    private val controller = ListController(this)

    override fun setupView() {
        super.setupView()
        recycler_view.apply {
            val linearLayoutManager = LinearLayoutManager(requireContext())
            layoutManager = linearLayoutManager
            setController(controller)
        }

        swipe_refresh.setOnRefreshListener {
            swipe_refresh.isRefreshing = false
            viewModel.loadTracks()
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        viewModel.tracks.observe(this, Observer { handleTracks(it) })
        viewModel.trackSaved.observe(this, EventObserver { handleSubmitAction(it) })
        viewModel.date.observe(this, Observer { controller.setDate(it) })
    }

    override fun setupInitialRequest() {
        super.setupInitialRequest()
        viewModel.getCachedDate()
        viewModel.loadTracks()
        viewModel.logCurrentDate()
    }

    override fun onTrackClicked(track: Track) {
        viewModel.saveTrack(track)
    }

    override fun onReloadList() {
        viewModel.loadTracks()
    }

    private fun handleTracks(state: State<List<Track>>){
        controller.setLoading(state is State.Loading)
        controller.setError(if (state is State.Error) "${state.error.localizedMessage}\nTap to retry" else "")
        controller.setTracks(if (state is State.Data) state.data else emptyList())
    }

    override fun handleOnBackPressed() {
        requireActivity().finish()
    }

    private fun handleSubmitAction(state : State<Unit>){
        if (state is State.Data){
            findNavController().ensureNavigate(
                destinationId = R.id.action_to_fragment_track_detail,
                currentScreenId = R.id.fragment_list)
        }
    }

}