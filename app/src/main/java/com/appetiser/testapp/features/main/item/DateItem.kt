package com.appetiser.testapp.features.main.item

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.appetiser.testapp.R
import com.appetiser.testapp.common.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_date)
abstract class DateItem : EpoxyModelWithHolder<DateItem.Holder>() {

    @EpoxyAttribute
    lateinit var date: String

    override fun bind(holder: Holder){
        holder.textDate.text = date
    }

    class Holder: KotlinEpoxyHolder() {

        val textDate by bind<TextView>(R.id.text_date)

    }
}