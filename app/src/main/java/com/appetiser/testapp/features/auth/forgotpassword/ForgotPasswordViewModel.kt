package com.appetiser.testapp.features.auth.forgotpassword

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.testapp.data.source.repository.AuthRepository
import com.appetiser.testapp.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ForgotPasswordViewModel @Inject constructor(
        private val repository: AuthRepository,
        private val schedulers: BaseSchedulerProvider,
        app: Application)
    : AndroidViewModel(app) {

    private val _state: MutableLiveData<ForgotPasswordState> by lazy {
        MutableLiveData<ForgotPasswordState>()
    }

    val state: LiveData<ForgotPasswordState>
        get() {
            return _state
        }

    fun forgotPassword(email: String): Disposable {
        _state.value = ForgotPasswordState.ShowProgressLoading
        return repository.forgotPassword(email)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _state.value = ForgotPasswordState.HideProgressLoading
                }
                .subscribe({
                    if (it) {
                        _state.value = ForgotPasswordState.Success(true)
                    }
                }, {
                    _state.value = ForgotPasswordState.Error(it)
                })
    }

}
