package com.appetiser.baseplate.domain.response.auth.emailcheck

import com.appetiser.baseplate.domain.response.BaseResponse


data class EmailCheckDataResponse(val data: EmailExistDataResponse) : BaseResponse()
