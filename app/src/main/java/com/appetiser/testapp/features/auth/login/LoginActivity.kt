package com.appetiser.testapp.features.auth.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.appetiser.testapp.R
import com.appetiser.testapp.base.DaggerBaseActivity
import com.appetiser.baseplate.domain.response.error.ResponseError
import com.appetiser.testapp.ext.enableWhen
import com.appetiser.testapp.ext.toast
import com.appetiser.testapp.features.auth.forgotpassword.ForgotPasswordActivity

class LoginActivity : DaggerBaseActivity() {

    companion object {
        fun openActivity(context: Context, email: String) {
            val intent = Intent(context, LoginActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            context.startActivity(intent)
        }

        private const val KEY_EMAIL = "email"
        private const val KEY_PASSWORD = "password"
    }

    private lateinit var viewModel: LoginViewModel

    private lateinit var btnContinue: AppCompatImageButton

    private lateinit var etEmail: AppCompatEditText

    private lateinit var etPassword: AppCompatEditText

    private lateinit var forgotPassword: AppCompatTextView

    private lateinit var toolbarView: Toolbar

    private lateinit var toolbarTitle: AppCompatTextView


    override fun getLayoutResources(): Int {
        return R.layout.activity_login
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews(savedInstanceState)
        setUpViewModels()
        setupToolbar()
    }

    /**
     * Found an issue using the kotlinx when access layouts from library
     *
     * https://stackoverflow.com/questions/34169562/unresolved-reference-kotlinx
     * https://stackoverflow.com/questions/48378696/unresolved-reference-for-synthetic-view-when-layout-is-in-library-module/54123767#54123767
     *
     * as a workaround will be using findViewById()
     */
    private fun setUpViews(savedInstanceState: Bundle?) {
        btnContinue = findViewById(R.id.btnContinue)
        etEmail = findViewById(R.id.etEmail)
        etPassword = findViewById(R.id.etPassword)
        forgotPassword = findViewById(R.id.forgotPassword)

        etEmail.apply {
            setText(savedInstanceState?.getString(LoginActivity.KEY_EMAIL))
            intent?.extras?.let {
                setText(it.getString(KEY_EMAIL, ""))
            }
        }

        etPassword.apply {
            setText(savedInstanceState?.getString(LoginActivity.KEY_PASSWORD))
        }

        btnContinue.enableWhen(etEmail) {
            Patterns.EMAIL_ADDRESS.matcher(it).matches() && it.isNotEmpty()
        }

        btnContinue.setOnClickListener {
            viewModel.login(etEmail.text.toString(), etPassword.text.toString())
        }

        forgotPassword.setOnClickListener {
            ForgotPasswordActivity.openActivity(this)
        }

    }

    private fun setupToolbar() {
        toolbarView = findViewById(R.id.toolbarView)
        toolbarTitle = toolbarView.findViewById(R.id.toolbarTitle)

        setSupportActionBar(toolbarView)

        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowCustomEnabled(true)
        }

        toolbarView.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setUpViewModels() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(LoginViewModel::class.java)

        viewModel.state.observe(this, Observer { it ->
            when (it) {
                is LoginState.LoginSuccess -> {
                    toast("Login successfull")
                }

                is LoginState.Error -> {
                    // show error message
                    ResponseError.getError(it.throwable,
                            ResponseError.ErrorCallback(httpExceptionCallback = {
                                toast("Error $it")
                            }))
                }
                is LoginState.ShowProgressLoading -> {
                    toast("Sending request")
                }
            }
        })

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_EMAIL, etEmail.text.toString())
        outState.putString(KEY_PASSWORD, etPassword.text.toString())
    }

} 
