package com.appetiser.testapp.api

import com.appetiser.testapp.features.main.TracksResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices  {

    @GET("search")
    fun getTracks(
        @Query("term") term: String,
        @Query("country") country: String,
        @Query("media") media: String
    ): Single<TracksResponse>

}
