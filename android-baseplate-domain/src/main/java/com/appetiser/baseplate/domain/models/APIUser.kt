package com.appetiser.baseplate.domain.models

import com.appetiser.baseplate.domain.models.base.APIBaseUser
import com.google.gson.annotations.SerializedName


class APIUser(
        @field:SerializedName("email_verified_at")
        val emailVerifiedAt: String? = "",
        @field:SerializedName("dob")
        val dateOfBirth: String? = ""
) : APIBaseUser()
