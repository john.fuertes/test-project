package com.appetiser.testapp.di

import com.appetiser.testapp.data.mapper.UserSessionMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MapperModule {

    @Provides
    @Singleton
    fun providesUserSessionMapper(): UserSessionMapper {
        return UserSessionMapper.getInstance()
    }

}
