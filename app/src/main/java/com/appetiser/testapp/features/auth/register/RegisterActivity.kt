package com.appetiser.testapp.features.auth.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.appetiser.testapp.BuildConfig
import com.appetiser.testapp.R
import com.appetiser.testapp.base.DaggerBaseActivity
import com.appetiser.testapp.data.poko.CountryCode
import com.appetiser.baseplate.domain.response.error.ResponseError
import com.appetiser.testapp.ext.enableWhen
import com.appetiser.testapp.ext.toast
import com.google.android.material.textfield.TextInputEditText
import timber.log.Timber

class RegisterActivity : DaggerBaseActivity() {

    companion object {
        fun openActivity(context: Context, email: String) {
            val intent = Intent(context, RegisterActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            context.startActivity(intent)
        }

        private const val KEY_EMAIL = "email"
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)
                .get(RegisterViewModel::class.java)
    }

    private var networkCountryIso = "US"
    private lateinit var countryCode: String
    private lateinit var toolbarView: Toolbar
    private lateinit var toolbarTitle: AppCompatTextView
    private lateinit var emailAddress: TextInputEditText
    private lateinit var mobileNumber: TextInputEditText
    private lateinit var password: TextInputEditText
    private lateinit var email: String
    private lateinit var btnContinue: AppCompatImageButton
    private lateinit var countryCodeSpinner: Spinner

    override fun getLayoutResources(): Int = R.layout.activity_onboard_register_step1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbarView = findViewById(R.id.toolbarView)
        toolbarTitle = toolbarView.findViewById(R.id.toolbarTitle)
        emailAddress = findViewById(R.id.etEmail)
        password = findViewById(R.id.etPassword)
        btnContinue = findViewById(R.id.btnContinue)
        countryCodeSpinner = findViewById(R.id.countryCodeSpinner)
        mobileNumber = findViewById(R.id.etMobile)

        setupToolbar()
        setUpViewModels()

        intent?.extras?.let {
            email = it.getString(KEY_EMAIL, savedInstanceState?.getString(KEY_EMAIL, ""))
        }

        initData()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_EMAIL, email)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbarView)

        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowCustomEnabled(true)
        }

        toolbarView.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setUpViewModels() {

        viewModel.state.observe(this, Observer { it ->
            when (it) {
                is RegisterState.SaveLoginCredentials -> {
                    toast("Register successfull")
                }

                is RegisterState.GetCountryCode -> {
                    if (it.countryCode.isNotEmpty()) {
                        mapCountryCodesToView(it.countryCode)
                        setCountryCodesListener(it.countryCode)
                    }
                }

                is RegisterState.Error -> {
                    // show error message
                    ResponseError.getError(it.throwable,
                            ResponseError.ErrorCallback(httpExceptionCallback = {
                                toast("Error $it")
                            }))
                }
                is RegisterState.ShowProgressLoading -> {
                    toast("Sending request")
                }
            }
        })

        viewModel.fetchCountryCode()
    }

    private fun initData() {
        networkCountryIso = getNetworkCountryIso()

        emailAddress.apply {
            isEnabled = false
            setText(email)
        }

        btnContinue.enableWhen(password) {
            it.isNotEmpty() && it.length >= 6
        }

        btnContinue.enableWhen(mobileNumber) {
            it.isNotEmpty()
        }

        btnContinue.setOnClickListener {
            viewModel.register(email = email, password = password.text.toString(), mobileNumber = mobileNumber.text.toString() )
        }

    }

    private fun getNetworkCountryIso(): String {
        // adb shell setprop gsm.sim.operator.iso-country ph
        if (BuildConfig.DEBUG) {
            return "ph".toUpperCase()
        }
        val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return tm.networkCountryIso.capitalize()
    }

    private fun mapCountryCodesToView(countryCodes: List<CountryCode>) {
        countryCodeSpinner.adapter = CountryAdapter(this, R.layout.item_country, R.id.countryCode, countryCodes)


        // find the country code & get the index
        val found = countryCodes.filter {
            it.flag.contains("$networkCountryIso.")
        }

        if (found.isNotEmpty()) {
            Timber.d("indexOf[$networkCountryIso]: ${countryCodes.indexOf(found[0])}")
            val index = countryCodes.indexOf(found[0])
            countryCodeSpinner.setSelection(index, true)
            countryCode = found[0].callingCode
        }
    }

    private fun setCountryCodesListener(countryCodes: List<CountryCode>) {
        countryCodeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val country = countryCodes[position]
                Timber.d("position: $position, countryCode: ${country.callingCode}")
                countryCode = country.callingCode
            }

        }
    }
}
