package com.appetiser.testapp.features.auth.landing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.appcompat.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.appetiser.testapp.R
import com.appetiser.testapp.base.DaggerBaseActivity
import com.appetiser.testapp.ext.enableWhen
import com.appetiser.testapp.ext.indicatorIcons
import com.appetiser.testapp.ext.toast
import com.appetiser.testapp.features.auth.login.LoginActivity
import com.appetiser.baseplate.domain.response.error.ResponseError
import com.appetiser.testapp.features.auth.register.RegisterActivity

class EmailCheckActivity : DaggerBaseActivity() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, EmailCheckActivity::class.java)
            context.startActivity(intent)
        }

        private const val EMAIL = "email"
    }

    private lateinit var viewModel: EmailCheckViewModel

    private lateinit var btnContinue: AppCompatImageButton

    private lateinit var etEmail: AppCompatEditText

    private lateinit var toolbarView: Toolbar

    private lateinit var toolbarTitle: AppCompatTextView

    override fun getLayoutResources(): Int {
        return R.layout.activity_onboard_email
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setUpViews(savedInstanceState)
        setupViewModel()
        setupClickListeners()
    }


    /**
     * Found an issue using the kotlinx when access layouts from library
     *
     * https://stackoverflow.com/questions/34169562/unresolved-reference-kotlinx
     * https://stackoverflow.com/questions/48378696/unresolved-reference-for-synthetic-view-when-layout-is-in-library-module/54123767#54123767
     *
     * as a workaround will be using findViewById()
     */
    private fun setUpViews(savedInstanceState: Bundle?) {
        btnContinue = findViewById(R.id.btnContinue)
        etEmail = findViewById(R.id.etEmail)
        etEmail.apply {
            indicatorIcons(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
            setText(savedInstanceState?.getString(EMAIL))
        }
    }

    private fun setupToolbar() {
        toolbarView = findViewById(R.id.toolbarView)
        toolbarTitle = toolbarView.findViewById(R.id.toolbarTitle)

        setSupportActionBar(toolbarView)

        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowCustomEnabled(true)
        }

        toolbarView.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(EmailCheckViewModel::class.java)

        viewModel.state.observe(this, Observer {
            when (it) {
                is EmailState.EmailExists -> {
                    LoginActivity.openActivity(this, it.email)
                }
                is EmailState.EmailDoesNotExist -> {
                    RegisterActivity.openActivity(this, it.email)
                }
                is EmailState.ShowProgressLoading -> {
                    toast("Loading...")
                }
                is EmailState.HideProgressLoading -> {
                }
                is EmailState.Error -> {
                    // show error message
                    ResponseError.getError(it.throwable,
                            ResponseError.ErrorCallback(httpExceptionCallback = {
                                toast("Error $it")
                            }))
                }
            }
        })
    }

    private fun setupClickListeners() {
        btnContinue.enableWhen(etEmail) { Patterns.EMAIL_ADDRESS.matcher(it).matches() }
        btnContinue.setOnClickListener {
            viewModel.checkEmail(etEmail.text.toString())
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(EMAIL, etEmail.text.toString())
    }

}
