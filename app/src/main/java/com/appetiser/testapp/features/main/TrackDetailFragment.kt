package com.appetiser.testapp.features.main

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.appetiser.testapp.R
import com.appetiser.testapp.base.BaseFragment
import com.appetiser.testapp.data.vo.Track
import com.appetiser.testapp.ext.ensureNavigate
import com.appetiser.testapp.ext.loadImageUrl
import kotlinx.android.synthetic.main.fragment_track_detail.*

class TrackDetailFragment : BaseFragment(R.layout.fragment_track_detail) {

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ViewModel::class.java)
    }

    override fun setupView() {
        super.setupView()
        toolbar.setNavigationOnClickListener { handleOnBackPressed() }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        viewModel.track.observe(this, Observer { handleTrackDetails(it) })
        viewModel.trackRemoved.observe(this,EventObserver { handleTrackRemoved(it) })
    }

    override fun setupInitialRequest() {
        super.setupInitialRequest()
        viewModel.loadTrack()
    }

    override fun handleOnBackPressed() {
        viewModel.removeTrack()
    }

    private fun handleTrackDetails(state: State<Track>){
        if (state is State.Data){
            image_artwork.loadImageUrl(state.data.imageUrlLarge)
            text_name.text = state.data.name
            text_description.text = state.data.longDescription
            text_genre.text = state.data.genre
            text_price.text = "${state.data.price} ${state.data.currency}"
        }
    }

    private fun handleTrackRemoved(state: State<Unit>){
        if (state is State.Data){
            findNavController().ensureNavigate(
                destinationId = R.id.action_to_fragment_list,
                currentScreenId = R.id.fragment_track_detail)
        }
    }

}