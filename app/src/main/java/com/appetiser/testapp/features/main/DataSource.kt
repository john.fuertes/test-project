package com.appetiser.testapp.features.main

import com.appetiser.testapp.data.vo.Track
import io.reactivex.Completable
import io.reactivex.Single

interface DataSource {

    fun getTracks(
        term: String,
        country: String,
        media: String
    ): Single<List<Track>>

    fun saveTrack(track: Track?): Completable

    fun setLogDate(date: String): Completable

    fun getLastLogDate(): Single<String>

    fun getTrack(): Single<Track>
}