package com.appetiser.testapp.di

import android.app.Application
import com.appetiser.testapp.TestAppApplication
import com.appetiser.testapp.di.builders.ActivityBuilder
import com.appetiser.testapp.di.builders.FragmentBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            (AndroidSupportInjectionModule::class),
            (MapperModule::class),
            (StorageModule::class),
            (NetworkModule::class),
            (ViewModelModernModule::class),
            (ActivityBuilder::class),
            (SchedulerModule::class),
            (ViewModelFactoryModule::class),
            (FragmentBuilder::class)
        ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: TestAppApplication)
}
