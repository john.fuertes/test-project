package com.appetiser.testapp.utils.schedulers

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers


/**
 * Implementation of the [BaseSchedulerProvider] making all [Scheduler]s immediate.
 */
class ImmediateSchedulerProvider : BaseSchedulerProvider {

    override fun computation(): Scheduler {
        return Schedulers.single()
    }

    override fun io(): Scheduler {
        return Schedulers.single()
    }

    override fun ui(): Scheduler {
        return Schedulers.single()
    }
}
