package com.appetiser.testapp.data.poko

data class Device(var width: Int, var height: Int)
