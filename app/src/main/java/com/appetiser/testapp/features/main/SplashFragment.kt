package com.appetiser.testapp.features.main

import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.appetiser.testapp.R
import com.appetiser.testapp.base.BaseFragment
import com.appetiser.testapp.ext.ensureNavigate

class SplashFragment : BaseFragment(R.layout.fragment_splash) {

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ViewModel::class.java)
    }

    override fun observeViewModel() {
        super.observeViewModel()
        viewModel.showInitialScreen.observe(this, EventObserver {
            findNavController().ensureNavigate(
                destinationId = if (it) R.id.action_to_fragment_list
                else R.id.action_to_fragment_track_detail,
                currentScreenId = R.id.fragment_splash
            )
        })
    }

    override fun setupInitialRequest() {
        super.setupInitialRequest()
        viewModel.checkLastScreen()
    }
}