package com.appetiser.baseplate.domain.response.country_codes

import com.appetiser.baseplate.domain.response.BaseResponse

data class CountryCodeResponse(val data: CountryCodeDataResponse) : BaseResponse()
