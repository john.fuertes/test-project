package com.appetiser.testapp.features.auth

import com.appetiser.android.baseplate.persistence.AppDatabase
import com.appetiser.testapp.data.mapper.UserSessionMapper
import com.appetiser.testapp.data.source.local.AuthLocalSource
import com.appetiser.testapp.data.source.remote.AuthRemoteSource
import com.appetiser.testapp.data.source.repository.AuthRepository
import com.appetiser.testapp.di.scopes.ActivityScope
import com.appetiser.baseplate.domain.BaseplateApiServices
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class AuthRepositoryModule {

    @ActivityScope
    @Provides
    fun providesAuthLocalSource(database: AppDatabase, mapper: UserSessionMapper):
            AuthLocalSource = AuthLocalSource(database, mapper)

    @ActivityScope
    @Provides
    fun providesAuthRemoteSource(baseplateApiServices: BaseplateApiServices, mapper: UserSessionMapper, gson: Gson):
            AuthRemoteSource = AuthRemoteSource(baseplateApiServices, mapper, gson)

    @ActivityScope
    @Provides
    fun providesAuthRepository(remote: AuthRemoteSource, local: AuthLocalSource): AuthRepository =
            AuthRepository(remote, local)

}
