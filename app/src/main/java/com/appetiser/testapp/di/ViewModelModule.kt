package com.appetiser.testapp.di

import androidx.lifecycle.ViewModel
import com.appetiser.testapp.di.scopes.ViewModelKey
import com.appetiser.testapp.features.auth.landing.EmailCheckViewModel
import com.appetiser.testapp.features.auth.forgotpassword.ForgotPasswordViewModel
import com.appetiser.testapp.features.auth.login.LoginViewModel
import com.appetiser.testapp.features.auth.register.RegisterViewModel
import com.appetiser.testapp.features.auth.register.verify.VerifyAccountViewModel
import com.appetiser.testapp.features.main.ViewModel as TracksViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModernModule {

    @Binds
    @IntoMap
    @ViewModelKey(EmailCheckViewModel::class)
    abstract fun bindEmailCheckViewModel(viewModel: EmailCheckViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordViewModel::class)
    abstract fun bindForgotPasswordViewModel(viewModel: ForgotPasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyAccountViewModel::class)
    abstract fun bindVerifyAccountViewModel(viewModel: VerifyAccountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    abstract fun bindVRegisterViewModel(viewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TracksViewModel::class)
    abstract fun bindTracksViewModel(viewModel: TracksViewModel): ViewModel

}
