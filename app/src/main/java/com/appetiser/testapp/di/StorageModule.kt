package com.appetiser.testapp.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.appetiser.android.baseplate.persistence.AppDatabase
import com.appetiser.testapp.common.PreferenceHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): PreferenceHelper {
        return PreferenceHelper(application)
    }

    @Provides
    @Singleton
    fun providesAppDatabase(application: Application): AppDatabase {
        return AppDatabase.getInstance(application.applicationContext)
    }

}
