package com.appetiser.testapp.di

import androidx.lifecycle.ViewModelProvider
import com.appetiser.testapp.ViewModelFactory
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Suppress("unused")
@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
