package com.appetiser.testapp.features.main

import android.annotation.SuppressLint
import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.testapp.base.BaseViewModel
import com.appetiser.testapp.data.vo.Track
import com.appetiser.testapp.utils.schedulers.BaseSchedulerProvider
import io.reactivex.rxkotlin.addTo
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ViewModel @Inject constructor(
    private val scheduler: BaseSchedulerProvider,
    private val repository: Repository,
    app: Application
) : BaseViewModel(app) {

    private val _tracks = MutableLiveData<State<List<Track>>>()
    val tracks: LiveData<State<List<Track>>> get() = _tracks

    private val _trackSaved = MutableLiveData<Event<State<Unit>>>()
    val trackSaved: LiveData<Event<State<Unit>>> get() = _trackSaved

    private val _date = MutableLiveData<String>()
    val date: LiveData<String> get() = _date

    private val _track = MutableLiveData<State<Track>>()
    val track: LiveData<State<Track>> get() = _track

    private val _trackRemoved = MutableLiveData<Event<State<Unit>>>()
    val trackRemoved: LiveData<Event<State<Unit>>> get() = _trackRemoved

    private val _showInitialScreen = MutableLiveData<Event<Boolean>>()
    val showInitialScreen: LiveData<Event<Boolean>> get() = _showInitialScreen

    fun loadTracks() {
        repository.getTracks(
            term = "star",
            country = "au",
            media = "movie"
        )
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .doOnSubscribe { _tracks.postValue(State.Loading) }
            .subscribe({
                _tracks.postValue(State.Data(it))
            }, {
                _tracks.postValue(State.Error(it))
            })
            .addTo(disposable)
    }

    fun saveTrack(track: Track) {
        repository.saveTrack(track)
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .doOnSubscribe { _trackSaved.postValue(Event(State.Loading)) }
            .subscribe({
                _trackSaved.postValue(Event(State.Data(Unit)))
            }, {
                _trackSaved.postValue(Event(State.Error(it)))
            })
            .addTo(disposable)
    }

    @SuppressLint("SimpleDateFormat")
    fun logCurrentDate() {
        val date = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat("MMM/dd/yyyy")
        repository.setLogDate(dateFormat.format(date))
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .subscribe()
            .addTo(disposable)
    }

    fun getCachedDate() {
        repository.getLastLogDate()
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .subscribe({
                _date.postValue(it)
            }, {
                it.printStackTrace()
            })
            .addTo(disposable)
    }

    fun loadTrack() {
        repository.getTrack()
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .doOnSubscribe { _track.postValue(State.Loading) }
            .subscribe({
                _track.postValue(State.Data(it))
            }, {
                _track.postValue(State.Error(it))
            })
            .addTo(disposable)
    }

    fun removeTrack() {
        repository.saveTrack(null)
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .doOnSubscribe { _trackRemoved.postValue(Event(State.Loading)) }
            .subscribe({
                _trackRemoved.postValue(Event(State.Data(Unit)))
            }, {
                _trackRemoved.postValue(Event(State.Error(it)))
            })
            .addTo(disposable)
    }

    fun checkLastScreen() {
        repository.getTrack()
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .subscribe({
                _showInitialScreen.postValue(Event(false))
            }, {
                _showInitialScreen.postValue(Event(true))
            })
            .addTo(disposable)
    }

}