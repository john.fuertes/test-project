package com.appetiser.testapp.data

import com.appetiser.testapp.data.poko.AccessToken
import com.appetiser.testapp.data.poko.UserSession
import com.appetiser.testapp.data.poko.CountryCode
import io.reactivex.Completable
import io.reactivex.Single

interface AuthSource {

    fun checkEmail(email: String): Single<Boolean>

    fun login(email: String, password: String): Single<Map<String, Any>>

    fun saveCredentials(user: UserSession): Single<UserSession>

    fun saveToken(token: String): Single<AccessToken>

    fun getUserSession(): Single<UserSession>

    fun register(email: String, password: String, confirmPassword: String, country: String, dob: String, mobileNumber: String,
                 firstName: String, lastName: String, address: String): Single<Map<String, Any>>

    fun verifyAccount(uid: String, code: String): Single<Boolean>

    fun resendVerificationCode(email: String): Single<Boolean>

    fun forgotPassword(email: String): Single<Boolean>

    fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>>

    fun logout(): Completable

}
