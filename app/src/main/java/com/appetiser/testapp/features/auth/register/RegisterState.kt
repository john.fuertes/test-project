package com.appetiser.testapp.features.auth.register

import com.appetiser.testapp.data.poko.CountryCode
import com.appetiser.testapp.data.poko.UserSession

sealed class RegisterState {
    data class SaveLoginCredentials(val user: UserSession) : RegisterState()
    data class GetCountryCode(val countryCode: List<CountryCode>) : RegisterState()
    data class Error(val throwable: Throwable): RegisterState()
    object ShowProgressLoading : RegisterState()
    object HideProgressLoading : RegisterState()
}
