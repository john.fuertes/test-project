package  com.appetiser.testapp.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.appetiser.testapp.di.Injectable
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var disposable: CompositeDisposable

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        val callback = object: OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                this@BaseFragment.handleOnBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this,callback)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        disposable = CompositeDisposable()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        observeViewModel()
        setupInitialRequest()
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }

    fun show(fragment: BaseFragment, content: Int, tag: String) {
        val transaction = fragmentManager?.beginTransaction()
        val prevFragment = fragmentManager?.findFragmentByTag(tag)
        if (prevFragment != null) {
            transaction?.remove(prevFragment)
        }
        transaction?.addToBackStack(null)
        transaction?.add(content, fragment, tag)?.commit()
    }

    open fun setupView() {}

    open fun observeViewModel() {}

    open fun setupInitialRequest() {}

    open fun handleOnBackPressed() {
        findNavController().navigateUp()
    }

}
