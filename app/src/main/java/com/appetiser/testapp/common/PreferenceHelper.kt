package com.appetiser.testapp.common

import android.content.Context
import androidx.core.content.edit
import com.appetiser.testapp.data.vo.Track
import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferenceHelper
@Inject
constructor(context: Context){

    private val sharedPreferences = context.getSharedPreferences("pref-v1", Context.MODE_PRIVATE)

    /**
     * Cache/Invalidate selected Track
     */
    fun setTrack(track: Track?){
        if (track == null){
            sharedPreferences.edit {
                remove("track")
            }
        }else {
            sharedPreferences.edit {
                putString("track", Gson().toJson(track))
            }
        }
    }

    /**
     * Retrieves Track
     */
    fun getTrack(): Track? {
        val json = sharedPreferences.getString("track", "")!!
        if (json.isEmpty()) {
            return null
        }
        return Gson().fromJson(json, Track::class.java)
    }

    /**
     * Cache date formatted string
     */
    fun setLastLogDate(date: String){
        sharedPreferences.edit{
            putString("date",date)
        }
    }

    /**
     * get date formatted string
     */
    fun getLastLogDate(): String{
        return sharedPreferences.getString("date","")!!
    }

}