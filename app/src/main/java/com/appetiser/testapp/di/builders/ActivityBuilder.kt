package com.appetiser.testapp.di.builders

import com.appetiser.testapp.di.scopes.ActivityScope
import com.appetiser.testapp.features.auth.AuthRepositoryModule
import com.appetiser.testapp.features.auth.landing.EmailCheckActivity
import com.appetiser.testapp.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.testapp.features.auth.login.LoginActivity
import com.appetiser.testapp.features.auth.register.RegisterActivity
import com.appetiser.testapp.features.auth.register.verify.VerifyAccountActivity
import com.appetiser.testapp.features.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeEmailCheckActivity(): EmailCheckActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeVerifyAccountActivity(): VerifyAccountActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

}
