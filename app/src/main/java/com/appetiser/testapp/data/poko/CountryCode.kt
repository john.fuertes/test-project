package com.appetiser.testapp.data.poko

import com.appetiser.testapp.di.NetworkModule

class CountryCode(
        val id: Long = 0,
        val name: String = "",
        val callingCode: String = "",
        val flag: String = ""
) {

    val flagUrl = "${NetworkModule.BASE_URL}$flag"
}
