package com.appetiser.testapp.data.source.local

import com.appetiser.android.baseplate.persistence.AppDatabase
import com.appetiser.android.baseplate.persistence.model.DBToken
import com.appetiser.android.baseplate.persistence.model.DBUserSession
import com.appetiser.testapp.data.AuthSource
import com.appetiser.testapp.data.mapper.UserSessionMapper
import com.appetiser.testapp.data.poko.AccessToken
import com.appetiser.testapp.data.poko.UserSession
import com.appetiser.testapp.data.poko.CountryCode
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthLocalSource @Inject
constructor(private val database: AppDatabase, private val mapper: UserSessionMapper) : AuthSource {

    override fun forgotPassword(email: String): Single<Boolean> {
        return Single.just(false)
    }

    override fun logout(): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun checkEmail(email: String): Single<Boolean> = Single.just(false)

    override fun login(email: String, password: String): Single<Map<String, Any>> = Single.just(mapOf())

    override fun saveCredentials(user: UserSession): Single<UserSession> {
        return Single.create { emitter ->
            val dbUser = DBUserSession(fullName = user.fullName,
                    firstName = user.firstName,
                    lastName = user.lastName,
                    email = user.email,
                    photoUrl = user.photoUrl,
                    uid = user.uid,
                    dateOfBirth = user.dateOfBirth,
                    emailVerifiedAt = user.emailVerifiedAt)
            database.userSessionDao().insert(dbUser)
            emitter.onSuccess(user)
        }
    }

    override fun saveToken(token: String): Single<AccessToken> {
        return Single.create { emitter ->
            database.tokenDao().insert(DBToken(token = token))
            emitter.onSuccess(AccessToken(token))
        }
    }

    override fun getUserSession(): Single<UserSession> {
        return database.userSessionDao().getUserInfo()
                .map {
                    mapper.mapFromDB(it)
                }
    }

    override fun register(email: String, password: String, confirmPassword: String, country: String, dob: String, mobileNumber:
    String, firstName: String, lastName: String, address: String): Single<Map<String, Any>> = Single.just(mapOf())

    override fun verifyAccount(uid: String, code: String): Single<Boolean> = Single.just(false)

    override fun resendVerificationCode(email: String): Single<Boolean> {
        return Single.just(false)
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
