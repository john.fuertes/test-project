package com.appetiser.baseplate.domain.models

import com.google.gson.annotations.SerializedName


class APICountryCode(
        val id: Long = 0,
        val name: String = "",
        @field:SerializedName("calling_code")
        val callingCode: String = "",
        val flag: String? = ""
)
