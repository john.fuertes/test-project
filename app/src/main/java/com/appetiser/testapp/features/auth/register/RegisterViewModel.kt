package com.appetiser.testapp.features.auth.register

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.testapp.base.BaseViewModel
import com.appetiser.testapp.data.mapper.getUserSession
import com.appetiser.testapp.data.source.repository.AuthRepository
import com.appetiser.baseplate.domain.ext.countryCode
import com.appetiser.testapp.utils.schedulers.BaseSchedulerProvider
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
        private val repository: AuthRepository,
        private val schedulers: BaseSchedulerProvider,
        app: Application)
    : BaseViewModel(app) {

    private val countryCode by lazy {
        app.applicationContext.countryCode()
    }

    private val _state: MutableLiveData<RegisterState> by lazy {
        MutableLiveData<RegisterState>()
    }

    val state: LiveData<RegisterState>
        get() {
            return _state
        }

    fun register(email: String, password: String, mobileNumber: String) {
        _state.value = RegisterState.ShowProgressLoading
        disposable.add(repository.register(email = email, password = password, confirmPassword = password, mobileNumber = mobileNumber, country = "", address = "", dob = "", firstName = "", lastName = "")
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _state.value = RegisterState.HideProgressLoading
                }
                .subscribe({
                    val user = it.getUserSession()

                    if (user.uid.isNotEmpty()) {
                        _state.value = RegisterState.SaveLoginCredentials(user)
                    }
                }, {
                    _state.value = RegisterState.Error(it)
                }))
    }

    fun fetchCountryCode() {
        val isCountryCodeValid = countryCode > 1
        val list = mutableListOf<Int>()

        if (isCountryCodeValid) {
            list.add(countryCode)
        }

        disposable.add(
                repository.getCountryCode(list)
                        .subscribeOn(schedulers.io())
                        .observeOn(schedulers.ui())
                        .subscribeBy(
                                onSuccess = {
                                    _state.value = RegisterState.GetCountryCode(it)
                                },
                                onError = {
                                    Timber.e("Error: $it")

                                })
        )
    }

}
