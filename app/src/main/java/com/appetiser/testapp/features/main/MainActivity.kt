package com.appetiser.testapp.features.main

import com.appetiser.testapp.R
import com.appetiser.testapp.base.DaggerBaseActivity

class MainActivity : DaggerBaseActivity() {

    override fun getLayoutResources(): Int {
        return R.layout.activity_main
    }
}
