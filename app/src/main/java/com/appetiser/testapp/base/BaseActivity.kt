package com.appetiser.testapp.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.crashlytics.android.Crashlytics
import dagger.android.AndroidInjection
import io.fabric.sdk.android.Fabric
import io.reactivex.disposables.CompositeDisposable


abstract class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getLayoutResources() > 0) {
            setContentView(getLayoutResources())
        }

        // TODO: uncomment the line below to setup fabric.
        //Fabric.with(this, Crashlytics())
    }

    abstract fun getLayoutResources(): Int

    override fun onStop() {
        super.onStop()
    }

}
