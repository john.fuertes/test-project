package com.appetiser.testapp.di

import com.appetiser.testapp.utils.schedulers.BaseSchedulerProvider
import com.appetiser.testapp.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()

}
