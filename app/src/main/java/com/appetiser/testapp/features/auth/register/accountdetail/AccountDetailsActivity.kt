package com.appetiser.testapp.features.auth.register.accountdetail

import com.appetiser.testapp.R
import com.appetiser.testapp.base.DaggerBaseActivity

class AccountDetailsActivity : DaggerBaseActivity() {

    override fun getLayoutResources(): Int = R.layout.activity_onboard_register_step3

}
