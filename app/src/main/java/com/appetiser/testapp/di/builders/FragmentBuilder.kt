package com.appetiser.testapp.di.builders

import com.appetiser.testapp.di.scopes.FragmentScope
import com.appetiser.testapp.features.main.ListFragment
import com.appetiser.testapp.features.main.SplashFragment
import com.appetiser.testapp.features.main.TrackDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesListFragment(): ListFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesSplashFragment(): SplashFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesTrackDetailFragment(): TrackDetailFragment

}