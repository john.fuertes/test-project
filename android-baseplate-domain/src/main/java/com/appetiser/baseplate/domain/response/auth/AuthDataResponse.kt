package com.appetiser.baseplate.domain.response.auth

import com.appetiser.baseplate.domain.response.BaseResponse

data class AuthDataResponse(val data: UserDataResponse) : BaseResponse()
