package com.appetiser.testapp.api

open class MetaData(
    val current_page: Int = 0,
    val first_page_url: String = "",
    val from: Int = 0,
    val last_page: Int = 0,
    val last_page_url: String = "",
    val next_page_url: String = "",
    val per_page: Int = 0,
    val prev_page_url: String = "",
    val to: Int = 0,
    val total: Int = 0,
    val current_total: Int = 0
)