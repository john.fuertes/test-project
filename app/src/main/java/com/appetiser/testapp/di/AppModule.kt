package com.appetiser.testapp.di

import android.content.Context
import com.appetiser.testapp.TestAppApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun providesApplicationContext(app: TestAppApplication) : Context
}
