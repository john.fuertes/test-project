package com.appetiser.testapp.features.main

import com.appetiser.testapp.data.vo.Track

data class TracksResponse(val results: List<Result>){
    /**
     * Mapper to Ui Model
     */
    fun toTracksData():List<Track>{
        val trackResults = mutableListOf<Track>()
        results.forEach {
            trackResults.add(it.toTrackData())
        }
        return trackResults
    }

}

data class Result(
    val trackName: String? = null,
    val artworkUrl60: String? = null,
    val artworkUrl100: String? = null,
    val trackPrice: Double? = null,
    val currency: String? = null,
    val primaryGenreName: String? = null,
    val shortDescription: String? = null,
    val longDescription: String? = null
){
    /**
     * Mapper to Ui Model
     */
    fun toTrackData():Track{
        return Track(
            name = trackName?:"Not Available",
            imageUrlLarge = artworkUrl100?:"",
            imageUrlSmall = artworkUrl60?:"",
            price = trackPrice?: 0.0,
            currency = currency?:"Not Available",
            shortDescription = shortDescription?:"Not Available",
            longDescription = longDescription?:"Not Available",
            genre = primaryGenreName?:"Not Available"
        )
    }
}
