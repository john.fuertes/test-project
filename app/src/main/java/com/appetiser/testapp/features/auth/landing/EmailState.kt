package com.appetiser.testapp.features.auth.landing


sealed class EmailState {

    data class EmailDoesNotExist(val email: String) : EmailState()

    data class EmailExists(val email: String) : EmailState()

    data class Error(val throwable: Throwable) : EmailState()

    object ShowProgressLoading : EmailState()

    object HideProgressLoading : EmailState()

}
