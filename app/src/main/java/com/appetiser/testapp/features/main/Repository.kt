package com.appetiser.testapp.features.main

import com.appetiser.testapp.api.ApiServices
import com.appetiser.testapp.common.PreferenceHelper
import com.appetiser.testapp.data.vo.Track
import io.reactivex.Completable
import io.reactivex.Single
import timber.log.Timber
import java.lang.Exception
import java.net.UnknownHostException
import javax.inject.Inject

class Repository @Inject constructor(
    private val api: ApiServices,
    private val preferenceHelper: PreferenceHelper
): DataSource{

    override fun getTracks(
        term: String,
        country: String,
        media: String
    ): Single<List<Track>> {
        return Single.defer {
            api.getTracks(term,country,media)
                .doAfterSuccess { Timber.d(it.results.size.toString()) }
                .map { it.toTracksData() }
                .onErrorResumeNext {
                    if (it is UnknownHostException){
                        Single.error(
                            Exception( "Uh oh! Looks like you lost your internet connection. " +
                                    "Please check your network settings and try again later.")
                        )
                    }
                    else {
                        Single.error(it)
                    }
                }
        }
    }

    override fun saveTrack(track: Track?): Completable {
        return Completable.fromAction {
            preferenceHelper.setTrack(track)
        }
    }

    override fun setLogDate(date: String): Completable {
        return Completable.fromAction {
            preferenceHelper.setLastLogDate(date)
        }
    }

    override fun getLastLogDate(): Single<String> {
        return Single.just(preferenceHelper.getLastLogDate())
    }

    override fun getTrack(): Single<Track> {
        return Single.defer {
            val track = preferenceHelper.getTrack()
            if (track == null){
                Single.error(Exception("No track cached."))
            }else {
                Single.just(track)
            }
        }
    }
}