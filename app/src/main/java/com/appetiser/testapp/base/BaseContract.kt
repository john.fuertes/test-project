package com.appetiser.testapp.base

interface BaseContract {
    fun showProgressLoading(visible: Boolean)
    fun showToast(message: String)
}
