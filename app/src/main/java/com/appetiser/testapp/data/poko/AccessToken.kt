package com.appetiser.testapp.data.poko

data class AccessToken(
        val token: String? = "",
        val refresh: String? = ""
) {

    val bearerToken get() = "Bearer $token"
}
